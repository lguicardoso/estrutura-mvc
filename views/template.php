<!DOCTYPE html>
<html lang="pt-br">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <link rel="stylesheet" type="text/css" href="<?php echo BASE_URL; ?>assets/css/style.css">
  <script type="text/javascript" src="<?php echo BASE_URL; ?>assets/js/script.js"></script>
  <title>Meu Site</title>
</head>
<body>
  <h1>Este é o topo!</h1>
  <a href="<?php echo $BASE_URL ?>">home</a>
  <hr/>
  <?php $this->loadViewInTemplate($viewName, $viewData); ?>
</body>
</html>